/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt6pro6;

/**
 *
 * @author alumno
 */
public class Interino extends Profesor {

	private String nrp;
	private String destino;

	/**
	 * @return the nrp
	 */
	public String getNrp() {
		return nrp;
	}

	/**
	 * @param nrp the nrp to set
	 */
	public void setNrp(String nrp) {
		this.nrp = nrp;
	}

	/**
	 * @return the destino
	 */
	public String getDestino() {
		return destino;
	}

	/**
	 * @param destino the destino to set
	 */
	public void setDestino(String destino) {
		this.destino = destino;
	}

	@Override
	public void setHoraslectivas(int horaslectivas) {
		this.horaslectivas = 10;
	}

}
