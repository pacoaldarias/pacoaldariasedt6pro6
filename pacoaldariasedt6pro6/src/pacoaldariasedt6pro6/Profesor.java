/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt6pro6;

/**
 *
 * @author alumno
 */
abstract class Profesor extends Persona {

	protected String idp;
	protected int horaslectivas;

	/**
	 * @return the idp
	 */
	public String getIdp() {
		return idp;
	}

	/**
	 * @param idp the idp to set
	 */
	public void setIdp(String idp) {
		this.idp = idp;
	}

	/**
	 * @return the horaslectivas
	 */
	public int getHoraslectivas() {
		return horaslectivas;
	}

	/**
	 * @param horaslectivas the horaslectivas to set
	 */
	public abstract void setHoraslectivas(int horaslectivas);

}
